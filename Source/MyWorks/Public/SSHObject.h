// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SSHObject.generated.h"

/**
 * 
 */
UCLASS()
class MYWORKS_API USSHObject : public UObject
{
	GENERATED_BODY()


	USSHObject();

public:
	UPROPERTY(EditAnywhere, Category = "Session")
	FString SpeakerName;

	UPROPERTY(VisibleAnywhere, Category = "Session")
	FDateTime SessionStart;

	UPROPERTY(VisibleAnywhere, Category = "Session")
	int SessionDuration;
};
