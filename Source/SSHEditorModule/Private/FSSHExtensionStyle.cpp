// Fill out your copyright notice in the Description page of Project Settings.

#include "FSSHExtensionStyle.h"
#include "SlateStyle.h"
#include "SlateStyleRegistry.h"
#include "SlateApplication.h"

TSharedPtr<FSlateStyleSet> FSSHExtensionStyle::StyleInstance = nullptr;

void FSSHExtensionStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FSSHExtensionStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

void FSSHExtensionStyle::ReloadTextures()
{
	FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

const ISlateStyle& FSSHExtensionStyle::Get()
{
	return *StyleInstance;
}

FName FSSHExtensionStyle::GetStyleSetName()
{
	static FName StyleName(TEXT("SSHExtensionStyle"));
	return StyleName;
}

#define IMAGE_BRUSH(RelativePath, ...) FSlateImageBrush(Style->RootToContentDir(RelativePath, TEXT(".png")), __VA_ARGS__)
const FVector2D Icon16x16(16.0f, 16.0f);
const FVector2D Icon20x20(20.0f, 20.0f);
const FVector2D Icon40x40(40.0f, 40.0f);


TSharedRef<FSlateStyleSet> FSSHExtensionStyle::Create()
{
	TSharedRef<FSlateStyleSet> Style = MakeShareable(new FSlateStyleSet("SSHExtensionStyle"));
	Style->SetContentRoot(FPaths::EngineContentDir() / TEXT("Editor/Slate"));

	Style->Set("SSHExtentions.Command1", new IMAGE_BRUSH("Icons/icon_Blueprint_SpawnActor_16x", Icon16x16));
	Style->Set("SSHExtentions.Command2", new IMAGE_BRUSH("Icons/icon_file_savelevels_16px", Icon16x16));
	Style->Set("SSHExtentions.Command3", new IMAGE_BRUSH("Icons/icon_file_ProjectOpen_16x", Icon16x16));
	Style->Set("SSHExtentions.Command4", new IMAGE_BRUSH("Icons/icon_file_ProjectsRecent_16px", Icon16x16));

	Style->Set("SSHToolbarIcon.Command1", new IMAGE_BRUSH("Icons/icon_toolbar_genericfinder_40px", Icon40x40));
	Style->Set("SSHToolbarIcon.Command1.Small", new IMAGE_BRUSH("Icons/icon_FoliageEditMode_LoadSettings_20px", Icon20x20));
	Style->Set("SSHToolbarIcon.Command2", new IMAGE_BRUSH("Icons/icon_ViewMode_BrushWireframe_40px", Icon40x40));
	Style->Set("SSHToolbarIcon.Command3", new IMAGE_BRUSH("Icons/icon_ViewMode_DetailLighting_40px", Icon40x40));
	Style->Set("SSHToolbarIcon.Command4", new IMAGE_BRUSH("Icons/icon_ViewMode_LightComplexity_40px", Icon40x40));

	return Style;
}

#undef IMAGE_BRUSH