// Fill out your copyright notice in the Description page of Project Settings.

#include "SSHObjectEditor.h"
#include "AssetEditorToolkit.h"
#include "IToolkitHost.h"
#include "Public/SSHObject.h"
#include "IToolkit.h"
#include "BaseToolkit.h"
#include "Public/FSSHExtensionStyle.h"
#include "PropertyEditorModule.h"
#include "ModuleManager.h"
#include "IDetailsView.h"
#include "SSHViewport.h"
#include "../AdvancedPreviewScene/Public/AdvancedPreviewSceneModule.h"

const FName FSSHObjectEditor::SSHEditorAppIdentifier = FName(TEXT("SSHObjectEditor"));
const FName FSSHObjectEditor::ViewportTabId = FName(TEXT("SSHViewport"));
const FName FSSHObjectEditor::DetailTabId = FName(TEXT("SSHDetail"));
const FName FSSHObjectEditor::PreviewSceneSettingsTabId = FName(TEXT("SSH PreviewScene Setting"));


#define LOCTEXT_NAMESPACE "SSHEditor"

FSSHObjectEditor::~FSSHObjectEditor() 
{
	DetailsView.Reset();
	AdvancedPreviewSettingsWidget.Reset();
}

void FSSHObjectEditor::InitSSHEditor(const EToolkitMode::Type Mode, const TSharedPtr<IToolkitHost>&	InitToolkitHost, USSHObject* InSSHObj)
{
	InSSHObj->SetFlags(EObjectFlags::RF_Transactional);
	SSHObj = InSSHObj;

	const bool bIsUpdatable = false;
	const bool bAllowFavorites = true;
	const bool bIsLockable = false;


	FPropertyEditorModule& PropertyEditorModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
	const FDetailsViewArgs DetailsViewArgs(bIsUpdatable, bIsLockable, true	, FDetailsViewArgs::ObjectsUseNameArea, false);
	DetailsView = PropertyEditorModule.CreateDetailView(DetailsViewArgs);

	Viewport = SNew(SSSHViewport)
		.ParentSSHEditor(SharedThis(this))
		.ObjectToEdit(InSSHObj);

	FAdvancedPreviewSceneModule& AdvancedPreviewSceneModule = FModuleManager::LoadModuleChecked<FAdvancedPreviewSceneModule>("AdvancedPreviewScene");
	AdvancedPreviewSettingsWidget = AdvancedPreviewSceneModule.CreateAdvancedPreviewSceneSettingsWidget(Viewport->GetPreviewScene());

//	const TSharedRef<FTabManager::FLayout> EditorDefaultLayout = FTabManager::NewLayout("SSHObjectEditor_v1")
	const TSharedRef<FTabManager::FLayout> EditorDefaultLayout = FTabManager::NewLayout("SSHObjectEditor_v2")
		->AddArea
		(
			FTabManager::NewPrimaryArea()->SetOrientation(Orient_Vertical)
			->Split
			(
				FTabManager::NewStack()
				->SetSizeCoefficient(0.1f)
				->AddTab(GetToolbarTabId(), ETabState::OpenedTab)
			)
			->Split
			(
				FTabManager::NewSplitter()->SetOrientation(Orient_Horizontal)
				->Split
				(
					FTabManager::NewStack()
					->SetSizeCoefficient(0.6f)
					->AddTab(ViewportTabId, ETabState::OpenedTab)
				)
				->Split
				(
					FTabManager::NewSplitter()
					->SetOrientation(Orient_Vertical)
					->Split
					(
						FTabManager::NewStack()
						->SetSizeCoefficient(0.4f)
						->AddTab(DetailTabId, ETabState::OpenedTab)
					)
					->Split
					(
						FTabManager::NewStack()
						->AddTab(PreviewSceneSettingsTabId, ETabState::OpenedTab)
					)
				)
			)
		);

	const bool bCreateDefaultStandaloneMenu = true;
	const bool bCreateDefaultToolbar = true;
	FAssetEditorToolkit::InitAssetEditor(
		Mode,
		InitToolkitHost,
		SSHEditorAppIdentifier,
		EditorDefaultLayout,
		bCreateDefaultStandaloneMenu,
		bCreateDefaultToolbar,
		InSSHObj
	);

	if (DetailsView.IsValid())
	{
		DetailsView->SetObject(SSHObj);
	}

}

void FSSHObjectEditor::RegisterTabSpawners(const TSharedRef<FTabManager>& TabManager)
{
	WorkspaceMenuCategory = TabManager->AddLocalWorkspaceMenuCategory(LOCTEXT("WorkspaceMenu_SSHAssetEditor", "SSH Asset Editor"));
	auto WorkspaceMenuCategoryRef = WorkspaceMenuCategory.ToSharedRef();

	FAssetEditorToolkit::RegisterTabSpawners(TabManager);

	TabManager->RegisterTabSpawner(ViewportTabId, FOnSpawnTab::CreateSP(this, &FSSHObjectEditor::SpawnTab_Viewport))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHExtentions.Command1"));

	TabManager->RegisterTabSpawner(DetailTabId, FOnSpawnTab::CreateSP(this, &FSSHObjectEditor::SpawnTab_Detail))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHExtentions.Command2"));

	TabManager->RegisterTabSpawner(PreviewSceneSettingsTabId, FOnSpawnTab::CreateSP(this, &FSSHObjectEditor::SpawnTab_PreviewSceneSettings))
		.SetDisplayName(LOCTEXT("PreviewSceneTab", "Preview Scene Settings"))
		.SetGroup(WorkspaceMenuCategoryRef)
		.SetIcon(FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHExtentions.Command2"));
}

void FSSHObjectEditor::UnregisterTabSpawners(const TSharedRef<FTabManager>& TabManager)
{
	FAssetEditorToolkit::UnregisterTabSpawners(TabManager);
	TabManager->UnregisterTabSpawner(ViewportTabId);
	TabManager->UnregisterTabSpawner(DetailTabId);
	TabManager->UnregisterTabSpawner(PreviewSceneSettingsTabId);
}

FName FSSHObjectEditor::GetToolkitFName() const
{
	return FName("SSHObjectEditor");
}

FText FSSHObjectEditor::GetBaseToolkitName() const
{
	return LOCTEXT("AppLabel", "SSHObjectEditor");
}

FString FSSHObjectEditor::GetWorldCentricTabPrefix() const
{
	return LOCTEXT("WorldCentricTabPrefix", "SSH").ToString();
}

FLinearColor FSSHObjectEditor::GetWorldCentricTabColorScale() const
{
	return FLinearColor(1.0, 0.0f, 0.0f, 0.5f);
}

FString FSSHObjectEditor::GetDocumentationLink() const
{
	return TEXT("Not available");
}

TSharedRef<SDockTab> FSSHObjectEditor::SpawnTab_Viewport(const FSpawnTabArgs& args)
{
	check(args.GetTabId() == ViewportTabId);
	return SNew(SDockTab)
		[
			Viewport.ToSharedRef()
		];
}

TSharedRef<SDockTab> FSSHObjectEditor::SpawnTab_Detail(const FSpawnTabArgs& args)
{
	check(args.GetTabId() == DetailTabId);
	return SNew(SDockTab)
		[
			DetailsView.ToSharedRef()
		];
}

TSharedRef<SDockTab> FSSHObjectEditor::SpawnTab_PreviewSceneSettings(const FSpawnTabArgs& args)
{
	check(args.GetTabId() == PreviewSceneSettingsTabId);
	return SNew(SDockTab)
		[
			AdvancedPreviewSettingsWidget.ToSharedRef()
		];
}

#undef LOCTEXT_NAMESPACE