// Fill out your copyright notice in the Description page of Project Settings.

#include "SSHExtensionCommands.h"
#include "UICommandInfo.h"
#include "InputChord.h"
#include "MessageDialog.h"
#include "Public/SSHObjectEditor.h"
#include "Public/SSHObject.h"

#define LOCTEXT_NAMESPACE "SSHCommand"

void FSSHExtensionCommands::RegisterCommands()
{
	UI_COMMAND(Command1, "SSHCommand1", "SSH Command1 Tooltip", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(Command2, "SSHCommand2", "SSH Command2 Tooltip", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(Command3, "SSHCommand3", "SSH Command3 Tooltip", EUserInterfaceActionType::Button, FInputGesture());
	UI_COMMAND(Command4, "SSHCommand4", "SSH Command4 Tooltip", EUserInterfaceActionType::Button, FInputGesture());
}

void FSSHExtentionActions::Action1()
{
	FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("SSHCommand", "SSH Editor Extention Action 1 Message"));
}

void FSSHExtentionActions::Action2()
{
	FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("SSHCommand", "SSH Editor Extention Action 2 Message"));
}

void FSSHExtentionActions::Action3()
{
//	FMessageDialog::Open(EAppMsgType::Ok, LOCTEXT("SSHCommand", "SSH Editor Extention Action 3 Message"));
	TSharedRef<FSSHObjectEditor> NewSSHObjectEditor(new FSSHObjectEditor());
	NewSSHObjectEditor->InitSSHEditor(EToolkitMode::Standalone, TSharedPtr<IToolkitHost>(), NewObject<USSHObject>());
}

#undef LOCTEXT_NAMESPACE