// Fill out your copyright notice in the Description page of Project Settings.

#include "SSHViewport.h"
#include "../AdvancedPreviewScene/Public/AdvancedPreviewScene.h"
#include "SSHViewportClient.h"
#include "Public/SSHObject.h"
#include "Components/StaticMeshComponent.h"
#include "SBoxPanel.h"
#include "STextBlock.h"

#define LOCTEXT_NAMESPACE "SSHViewport"

SSSHViewport::SSSHViewport()
	:PreviewScene(MakeShareable(new FAdvancedPreviewScene(FPreviewScene::ConstructionValues())))
{
}

SSSHViewport::~SSSHViewport()
{
	if (SSHViewportClient.IsValid())
	{
		SSHViewportClient->Viewport = nullptr;
	}
}

void SSSHViewport::Construct(const FArguments& InArgs)
{
	SSHEditorPtr = InArgs._ParentSSHEditor;
	SSHObj = InArgs._ObjectToEdit;

	SEditorViewport::Construct(SEditorViewport::FArguments());

	ViewportOverlay->AddSlot()
		.VAlign(VAlign_Top)
		.HAlign(HAlign_Left)
		.Padding(FMargin(10.0f, 10.0f, 10.0f, 10.0f))
		[
			SAssignNew(OverlayTextVerticalBox, SVerticalBox)
		];

	OverlayTextVerticalBox->ClearChildren();
	OverlayTextVerticalBox->AddSlot()
		[
			SNew(STextBlock)
			.Text(LOCTEXT("SSH WelcomeText", "Welcome to Stronghold"))
			.TextStyle(FEditorStyle::Get(), TEXT("TextBlock.ShadowedText"))
			.ColorAndOpacity(FLinearColor::Red)
		];

	UStaticMesh* StaticMesh = LoadObject<UStaticMesh>(nullptr, TEXT("/Engine/EngineMeshes/SM_MatPreviewMesh_01.SM_MatPreviewMesh_01"), nullptr, LOAD_None, nullptr);
	UMaterialInterface* BaseMaterial = LoadObject<UMaterialInterface>(nullptr, TEXT("/Engine/EditorMeshes/ColorCalibrator/M_ChromeBall.M_ChromeBall"), nullptr, LOAD_None, nullptr);

	PreviewMeshComponent = NewObject<UStaticMeshComponent>(GetTransientPackage(), NAME_None, EObjectFlags::RF_Transient);
	PreviewMeshComponent->SetStaticMesh(StaticMesh);
	PreviewMeshComponent->SetMaterial(0, BaseMaterial);

	FTransform Transform = FTransform::Identity;
	PreviewScene->AddComponent(PreviewMeshComponent, Transform);

	PreviewMeshComponent->SetSimulatePhysics(true);
}

void SSSHViewport::AddReferencedObjects(FReferenceCollector& Collector)
{
	Collector.AddReferencedObject(SSHObj);
	Collector.AddReferencedObject(PreviewMeshComponent);
}

TSharedRef<class FAdvancedPreviewScene> SSSHViewport::GetPreviewScene()
{
	return PreviewScene.ToSharedRef();
}

TSharedRef<FEditorViewportClient> SSSHViewport::MakeEditorViewportClient()
{
	SSHViewportClient = MakeShareable(new FSSHViewportClient(SSHEditorPtr, PreviewScene.ToSharedRef(), SharedThis(this), SSHObj));
	
	return SSHViewportClient.ToSharedRef();
}

#undef LOCTEXT_NAMESPACE