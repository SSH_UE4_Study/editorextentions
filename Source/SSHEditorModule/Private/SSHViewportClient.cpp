// Fill out your copyright notice in the Description page of Project Settings.

#include "SSHViewportClient.h"
#include "Public/SSHViewport.h"
#include "../AdvancedPreviewScene/Public/AdvancedPreviewScene.h"
#include "Public/SSHObjectEditor.h"

FSSHViewportClient::FSSHViewportClient(TWeakPtr<FSSHObjectEditor> ParentSSHEditor, const TSharedRef<FAdvancedPreviewScene>& AdvPreviewScene, const TSharedRef<SSSHViewport>& SSHViewport, USSHObject* ObjectToEdit)
	:FEditorViewportClient(nullptr, &AdvPreviewScene.Get(), StaticCastSharedRef<SEditorViewport>(SSHViewport))
	, SSHObjEditorPtr(ParentSSHEditor)
	, SSHEditorViewportPtr(SSHViewport)
	, SSHObject(ObjectToEdit)
{
	SetViewMode(EViewModeIndex::VMI_Lit);
	AdvancedPreviewScene = static_cast<FAdvancedPreviewScene*>(PreviewScene);

	SetViewLocation(FVector(0.0f, 3.0f, 2.0f));
	SetViewRotation(FRotator(-45.0f, -90.0f, 0.0f));
	SetViewLocationForOrbiting(FVector::ZeroVector, 500.0f);
}

FSSHViewportClient::~FSSHViewportClient()
{
}


void FSSHViewportClient::Tick(float DeltaSeconds)
{
	FEditorViewportClient::Tick(DeltaSeconds);
}

void FSSHViewportClient::Draw(const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	FEditorViewportClient::Draw(View, PDI);
}