// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AssetEditorToolkit.h"

/**
 * 
 */
class SSHEDITORMODULE_API FSSHObjectEditor : public FAssetEditorToolkit
{
public:
	FSSHObjectEditor() { ; }
	~FSSHObjectEditor();

	void InitSSHEditor(const EToolkitMode::Type Mode, const TSharedPtr<class IToolkitHost>&	InitToolkitHost, class USSHObject* InSSHObj);

	virtual void RegisterTabSpawners(const TSharedRef<FTabManager>& TabManager) override;
	virtual void UnregisterTabSpawners(const TSharedRef<FTabManager>& TabManager) override;
	virtual FName GetToolkitFName() const override;
	virtual FText GetBaseToolkitName() const override;
	virtual FString GetWorldCentricTabPrefix() const override;
	virtual FLinearColor GetWorldCentricTabColorScale() const;
	virtual FString GetDocumentationLink() const;

private:
	TSharedRef<SDockTab> SpawnTab_Viewport(const FSpawnTabArgs& args);
	TSharedRef<SDockTab> SpawnTab_Detail(const FSpawnTabArgs& args);
	TSharedRef<SDockTab> SpawnTab_PreviewSceneSettings(const FSpawnTabArgs& args);

private:
	static const FName SSHEditorAppIdentifier;
	static const FName ViewportTabId;
	static const FName DetailTabId;
	static const FName PreviewSceneSettingsTabId;

	TSharedPtr<class IDetailsView> DetailsView;

	TSharedPtr<class SSSHViewport> Viewport;

	class USSHObject* SSHObj;

	TSharedPtr<SWidget> AdvancedPreviewSettingsWidget;
};
