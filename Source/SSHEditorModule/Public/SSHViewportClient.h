// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EditorViewportClient.h"

/**
 * 
 */
class USSHObject;

class SSHEDITORMODULE_API FSSHViewportClient : public FEditorViewportClient, public TSharedFromThis<FSSHViewportClient>
{
public:
	FSSHViewportClient(TWeakPtr<class FSSHObjectEditor> ParentSSHEditor, const TSharedRef<class FAdvancedPreviewScene>& AdvPreviewScene, const TSharedRef<class SSSHViewport>& SSHViewport, USSHObject* ObjectToEdit);
	~FSSHViewportClient();

	virtual void Tick(float DeltaSeconds) override;
	virtual void Draw(const FSceneView* View, FPrimitiveDrawInterface* PDI) override;

private:
	TWeakPtr<class FSSHObjectEditor> SSHObjEditorPtr;
	TWeakPtr<class SSSHViewport> SSHEditorViewportPtr;
	USSHObject* SSHObject;
	class FAdvancedPreviewScene* AdvancedPreviewScene;
};
