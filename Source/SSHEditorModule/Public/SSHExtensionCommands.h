// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Commands.h"
#include "UICommandInfo.h"
#include "FSSHExtensionStyle.h"

/**
 * 
 */
class SSHEDITORMODULE_API FSSHExtensionCommands : public TCommands<FSSHExtensionCommands>
{
public:
	FSSHExtensionCommands()
		: TCommands<FSSHExtensionCommands>(
			TEXT("SSHExtentions"),
			NSLOCTEXT("SSHExtentions", "SSHExtentions", "SSHExtentions"),
			NAME_None, 
			FSSHExtensionStyle::GetStyleSetName())
	{

	}

	~FSSHExtensionCommands() { ; }

	virtual void RegisterCommands() override;

public:
	TSharedPtr<FUICommandInfo> Command1;
	TSharedPtr<FUICommandInfo> Command2;
	TSharedPtr<FUICommandInfo> Command3;
	TSharedPtr<FUICommandInfo> Command4;
};

class FSSHExtentionActions
{
public:
	static void Action1();
	static void Action2();
	static void Action3();
};
