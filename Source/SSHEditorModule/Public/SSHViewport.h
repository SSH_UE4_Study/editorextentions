// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "SEditorViewport.h"
#include "GCObject.h"

/**
 * 
 */
class FSSHObjectEditor;
class USSHObject;

class SSHEDITORMODULE_API SSSHViewport : public SEditorViewport, public FGCObject
{
public:
	SLATE_BEGIN_ARGS(SSSHViewport){}
		SLATE_ARGUMENT(TWeakPtr<FSSHObjectEditor>, ParentSSHEditor)
		SLATE_ARGUMENT(USSHObject*, ObjectToEdit)
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs);
	SSSHViewport();
	~SSSHViewport();

	virtual void AddReferencedObjects(FReferenceCollector& Collector) override;
	TSharedRef<class FAdvancedPreviewScene> GetPreviewScene();

protected:
	virtual TSharedRef<FEditorViewportClient> MakeEditorViewportClient() override;

private:
	TWeakPtr<FSSHObjectEditor> SSHEditorPtr;
	TSharedPtr<class FAdvancedPreviewScene> PreviewScene;
	TSharedPtr<class FSSHViewportClient> SSHViewportClient;
	USSHObject* SSHObj;

	TSharedPtr<SVerticalBox> OverlayTextVerticalBox;

	class UStaticMeshComponent* PreviewMeshComponent;
};
