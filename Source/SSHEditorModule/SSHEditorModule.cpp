// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "SSHEditorModule.h"
#include "Modules/ModuleManager.h"
#include "Public/SSHExtensionCommands.h"
#include "UICommandList.h"
#include "MultiBoxBuilder.h"
#include "LevelEditor.h"
#include "Public/FSSHExtensionStyle.h"

IMPLEMENT_MODULE(FSSHEditorModule, "SSHEditorModule" );
 
#define LOCTEXT_NAMESPACE "SSHMenu"

void FSSHEditorModule::StartupModule()
{
	FSSHExtensionStyle::Initialize();
	FSSHExtensionStyle::ReloadTextures();

	FSSHExtensionCommands::Register();

	SSHCommandList = MakeShareable(new FUICommandList());
	SSHCommandList->MapAction(FSSHExtensionCommands::Get().Command1, FExecuteAction::CreateStatic(&FSSHExtentionActions::Action1), FCanExecuteAction());
	SSHCommandList->MapAction(FSSHExtensionCommands::Get().Command2, FExecuteAction::CreateStatic(&FSSHExtentionActions::Action2), FCanExecuteAction());
	SSHCommandList->MapAction(FSSHExtensionCommands::Get().Command3, FExecuteAction::CreateStatic(&FSSHExtentionActions::Action3), FCanExecuteAction());
	SSHCommandList->MapAction(FSSHExtensionCommands::Get().Command4, FExecuteAction::CreateStatic(&FSSHExtentionActions::Action3), FCanExecuteAction());

	struct SSHMenu 
	{
		static void CreateSSHMenu(FMenuBuilder& MenuBuilder)
		{
			MenuBuilder.BeginSection("SSHSection1", LOCTEXT("SSHMenu", "SSH Menu Section1"));
// 				MenuBuilder.AddMenuEntry(FSSHExtensionCommands::Get().Command1,
// 					NAME_None, TAttribute<FText>(), TAttribute<FText>(), FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHExtentions.Command1"), NAME_None);
				MenuBuilder.AddMenuEntry(FSSHExtensionCommands::Get().Command1);
				MenuBuilder.AddMenuEntry(FSSHExtensionCommands::Get().Command2);
			MenuBuilder.EndSection();

			MenuBuilder.BeginSection("SSHSection2", LOCTEXT("SSHMenu", "SSH Menu Section2"));
			{
				struct SSHSubMenu
				{
					static void CreateSSHSubMenu(FMenuBuilder& SubMenuBuilder)
					{
						SubMenuBuilder.AddMenuEntry(FSSHExtensionCommands::Get().Command3);
						SubMenuBuilder.AddMenuEntry(FSSHExtensionCommands::Get().Command4);
					}
				};

				MenuBuilder.AddSubMenu(
					LOCTEXT("SSHMenu", "SSH SubMenu"),
					LOCTEXT("SSHMenu", "SSH SubMenu Tooltip"),
					FNewMenuDelegate::CreateStatic(&SSHSubMenu::CreateSSHSubMenu),
					false,
					FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHExtentions.Command1")
				);
			}
			MenuBuilder.EndSection();
		}
	};

	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");

	TSharedPtr<FExtender> SSHMenuExtender = MakeShareable(new FExtender());
	SSHMenuExtender->AddMenuExtension("WindowLayout", EExtensionHook::After, SSHCommandList, FMenuExtensionDelegate::CreateStatic(&SSHMenu::CreateSSHMenu));

	LevelEditorModule.GetMenuExtensibilityManager()->AddExtender(SSHMenuExtender);

	struct SSHToolbar
	{
		static void CretaeSSHToolbar(FToolBarBuilder& ToolbarBuilder)
		{
			ToolbarBuilder.BeginSection("SSHToolbar");
			{
// 				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command1);
// 				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command2);
// 				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command3);
// 				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command4);
				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command1,
					NAME_None, TAttribute<FText>(), TAttribute<FText>(), FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHToolbarIcon.Command1"), NAME_None);
				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command2,
					NAME_None, TAttribute<FText>(), TAttribute<FText>(), FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHToolbarIcon.Command2"), NAME_None);
				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command3,
					NAME_None, TAttribute<FText>(), TAttribute<FText>(), FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHToolbarIcon.Command3"), NAME_None);
				ToolbarBuilder.AddToolBarButton(FSSHExtensionCommands::Get().Command4,
					NAME_None, TAttribute<FText>(), TAttribute<FText>(), FSlateIcon(FSSHExtensionStyle::GetStyleSetName(), "SSHToolbarIcon.Command4"), NAME_None);
			}
			ToolbarBuilder.EndSection();
		}
	};


	TSharedPtr<FExtender> SSHToolbarExtender = MakeShareable(new FExtender());
	SSHToolbarExtender->AddToolBarExtension("Settings", EExtensionHook::After, SSHCommandList, FToolBarExtensionDelegate::CreateStatic(&SSHToolbar::CretaeSSHToolbar));

	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(SSHToolbarExtender);
}

void FSSHEditorModule::ShutdownModule()
{
	FSSHExtensionCommands::Unregister();

	FSSHExtensionStyle::Shutdown();
}

#undef LOCTEXT_NAMESPACE