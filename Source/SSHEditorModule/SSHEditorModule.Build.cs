// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SSHEditorModule : ModuleRules
{
	public SSHEditorModule(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Slate",
            "SlateCore",
            "LevelEditor",
            "UnrealEd",
            "MyWorks",
            "Engine",
            "AdvancedPreviewScene",
            "EditorStyle"
        });
	}
}
